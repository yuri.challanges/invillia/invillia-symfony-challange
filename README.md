# Invillia Challange

##### Setup / Instalação: 

`
composer install
`

##### Documentation under construction / Documentação em construção

###### api/doc

> Documentation generate using [NelmioApiDocBundle](https://github.com/nelmio/NelmioApiDocBundle)
> Documentação gerada utilizando [NelmioApiDocBundle](https://github.com/nelmio/NelmioApiDocBundle)

---
##### Routes / Rotas:

###### api/person
###### api/person/:id
###### api/shiporder
###### api/shiporder/:id

---

##### Importation form / Formulario de importação

> Import people and ship orders, saving at paths / Importa pessoas e pedidos, salvando nos caminhos:
> - public/uploads/people/people.xml
> - public/uploads/shiporders/shiporders.xml
> 
> If the processing option is checked, save the data in the database, creating or replacing them as necessary / Se a opção de procesamento for marcada salva os dados no banco, criando ou substituindo conforme necessario. 