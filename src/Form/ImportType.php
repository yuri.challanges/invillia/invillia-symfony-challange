<?php

namespace App\Form;

use App\Dto\ImportDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('people',FileType::class)
	        ->add('shiporders',FileType::class)
	        ->add('process',ChoiceType::class,[
	        	'expanded' => true,
	        	'choices' => [true,false],
		        'choice_label' => function($value){
			        return $value ? "Yes" : "No";
		        }
	        ])
	        ->add('submit',SubmitType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        	'data_class' => ImportDto::class
            // Configure your form options here
        ]);
    }
}
