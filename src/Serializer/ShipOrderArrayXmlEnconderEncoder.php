<?php

namespace App\Serializer;


use App\Entity\ShipItem;
use App\Entity\ShipOrder;
use App\Repository\PersonRepository;
use App\Repository\ShipOrderRepository;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ShipOrderArrayXmlEnconderEncoder extends XmlEncoder{
	public const FORMAT = 'xml';

	/**
	 * @var PersonRepository
	 */
	private $personRepository;
	/**
	 * @var ShipOrderRepository
	 */
	private $orderRepository;

	/**
	 * ShipOrderArrayXmlEnconderEncoder constructor.
	 *
	 * @param PersonRepository $personRepository
	 */
	public function __construct(PersonRepository $personRepository,ShipOrderRepository $orderRepository){
		parent::__construct();
		$this->personRepository = $personRepository;
		$this->orderRepository = $orderRepository;
	}

	public function decode($data,$format,array $context = []){
		$decoded = parent::decode($data,$format,$context);

//	    return [];
		return array_map(
			function($order){
//	    	dump($order);
				$newOrder = $this->orderRepository->find($order['orderid']);
				if($newOrder === null){
					$newOrder = new ShipOrder();
				}
				$newOrder->setId($order['orderid']);
				$newOrder->setPerson($this->personRepository->find($order['orderperson']));
				$items = $order['items']['item'];
				if(!isset($items[0])){
					$items = [$items];
				}

				foreach($newOrder->getItems() as $item){
					$newOrder->removeItem($item);
				}

				foreach($items as $item){

					$newItem = new ShipItem();
					$newItem->setPrice($item['price']);
					$newItem->setTitle($item['title']);
					$newItem->setNote($item['note']);
					$newItem->setQuantity($item['quantity']);

					$newOrder->addItem($newItem);
				}

				$newOrder->getShipTo()
					->setName($order['shipto']['name'])
					->setAddress($order['shipto']['address'])
					->setCity($order['shipto']['city'])
					->setCountry($order['shipto']['country']);

				return $newOrder;
			},
			$decoded['shiporder']
		);
	}

	public function encode($data,$format,array $context = []){
		// TODO: return your encoded data
		return 'b';
	}

	public function supportsDecoding($format)
	:bool{
		return self::FORMAT === $format;
	}

	public function supportsEncoding($format)
	:bool{
		return false;
	}
}
