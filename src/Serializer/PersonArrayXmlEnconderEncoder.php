<?php

namespace App\Serializer;

use App\Entity\Phone;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Intl\Exception\MethodNotImplementedException;

class PersonArrayXmlEnconderEncoder extends XmlEncoder
{
    public const FORMAT = 'xml';

	/**
	 * @var PersonRepository
	 */
	private $personRepository;

	/**
	 * PersonArrayXmlEnconderEncoder constructor.
	 *
	 * @param PersonRepository $personRepository
	 */
	public function __construct(PersonRepository $personRepository){
		parent::__construct();
		$this->personRepository = $personRepository;
	}

	public function encode($data, $format, array $context = [])
	{
		// TODO: return your encoded data
		return 'a';
	}

	public function supportsEncoding($format): bool
	{
		return false;
	}

    public function decode($data, $format, array $context = [])
    {
	    $decoded = parent::decode($data,$format,$context);

        return array_map(function($person){
	        $newPerson = $this->personRepository->find($person['personid']);
	        if($newPerson === null){
		        $newPerson = new Person();
	        }
	        $newPerson->setId($person['personid']);
	        $newPerson->setName($person['personname']);
	        $phones = $person['phones']['phone'];
	        if(!is_array($phones)){
	        	$phones = [$phones];
	        }

	        $newPerson->getContact()->setPhones($phones);

	        return $newPerson;
        },$decoded['person']);
    }

    public function supportsDecoding($format): bool
    {
        return self::FORMAT === $format;
    }
}
