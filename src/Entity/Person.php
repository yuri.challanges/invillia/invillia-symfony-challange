<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups("default")
     * @SerializedName("personid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("default")
     * @SerializedName("personname")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ShipOrder::class, mappedBy="person", orphanRemoval=true)
     */
    private $shipOrders;

	/**
	 * @var Contact
	 * @ORM\Embedded(class=Contact::class)
	 * @Groups("default")
	 * @SerializedName("phones")
	 */
    private $contact;

    public function __construct()
    {
        $this->shipOrders = new ArrayCollection();
        $this->contact = new Contact();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
    	$this->id = (int) $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ShipOrder[]
     */
    public function getShipOrders(): Collection
    {
        return $this->shipOrders;
    }

    public function addShipOrder(ShipOrder $shipOrder): self
    {
        if (!$this->shipOrders->contains($shipOrder)) {
            $this->shipOrders[] = $shipOrder;
            $shipOrder->setPerson($this);
        }

        return $this;
    }

    public function removeShipOrder(ShipOrder $shipOrder): self
    {
        if ($this->shipOrders->removeElement($shipOrder)) {
            // set the owning side to null (unless already changed)
            if ($shipOrder->getPerson() === $this) {
                $shipOrder->setPerson(null);
            }
        }

        return $this;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setContact(Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }
}
