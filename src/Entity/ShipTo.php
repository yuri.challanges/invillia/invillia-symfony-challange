<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class ShipTo
 * @package Entity
 * @ORM\Embeddable()
 */
class ShipTo{
	/**
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 */
	private $address;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 */
	private $city;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 */
	private $country;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }
}