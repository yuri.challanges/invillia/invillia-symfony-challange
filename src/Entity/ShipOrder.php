<?php

namespace App\Entity;

use App\Repository\ShipOrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=ShipOrderRepository::class)
 */
class ShipOrder
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @SerializedName("orderid")
     * @Groups("default")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="shipOrders")
     * @ORM\JoinColumn(nullable=false)
     * @SerializedName("orderperson")
     * @Groups("default")
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity=ShipItem::class, mappedBy="shipOrder", orphanRemoval=true,cascade={"persist"})
     * @Groups("default")
     */
    private $items;

	/**
	 * @var ShipTo
	 * @ORM\Embedded(class=ShipTo::class)
	 * @Groups("default")
	 * @SerializedName("shipto")
	 */
    private $shipTo;

	public function setId($id)
      	{
      		$this->id = (int) $id;
      
      		return $this;
      	}

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->shipTo = new ShipTo();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|ShipItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ShipItem $shipItem): self
    {
        if (!$this->items->contains($shipItem)) {
            $this->items[] = $shipItem;
            $shipItem->setShipOrder($this);
        }

        return $this;
    }

    public function removeItem(ShipItem $shipItem): self
    {
        if ($this->items->removeElement($shipItem)) {
            // set the owning side to null (unless already changed)
            if ($shipItem->getShipOrder() === $this) {
                $shipItem->setShipOrder(null);
            }
        }

        return $this;
    }

    public function getShipTo(): ShipTo
    {
        return $this->shipTo;
    }

    public function setShipTo(ShipTo $shipTo): self
    {
        $this->shipTo = $shipTo;

        return $this;
    }
}
