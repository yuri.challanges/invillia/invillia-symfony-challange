<?php

namespace App\Entity;

use App\Repository\ShipItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ShipItemRepository::class)
 */
class ShipItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("default")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("default")
     */
    private $note;

    /**
     * @ORM\Column(type="integer")
     * @Groups("default")
     */
    private $quantity;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups("default")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=ShipOrder::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shipOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getShipOrder(): ?ShipOrder
    {
        return $this->shipOrder;
    }

    public function setShipOrder(?ShipOrder $shipOrder): self
    {
        $this->shipOrder = $shipOrder;

        return $this;
    }
}
