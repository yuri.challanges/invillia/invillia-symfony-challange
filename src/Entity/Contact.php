<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Class Phones
 * @package App\Entity
 * @ORM\Embeddable()
 */
class Contact{
	/**
	 * @var string[]
	 * @ORM\Column(type="json")
	 * @Groups("default")
	 * @SerializedName("phone")
	 */
	protected $phones;

    public function getPhones(): ?array
    {
        return $this->phones;
    }

    public function setPhones(array $phones): self
    {
        $this->phones = $phones;

        return $this;
    }
}