<?php

namespace Dto;


use App\Entity\ShipOrder;
use Symfony\Component\Serializer\Annotation\Groups;

class ShipordersDto{
	/**
	 * @var ShipOrder[]
	 * @Groups("default")
	 */
	public $shiporder;

	/**
	 * ShipordersDto constructor.
	 *
	 * @param ShipOrder[] $shiporders
	 */
	public function __construct(array $shiporders){
		$this->shiporder = $shiporders;
	}

}