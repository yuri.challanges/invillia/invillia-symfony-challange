<?php

namespace App\Dto;


use App\Entity\Person;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class PeopleDto
 * @package App\Dto
 */
class PeopleDto{
	/**
	 * @var Person[]
	 * @Groups("default")
	 */
	public $person;

	/**
	 * PeopleDto constructor.
	 *
	 * @param Person[] $people
	 */
	public function __construct(array $people){
		$this->person = $people;
	}

}