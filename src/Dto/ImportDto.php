<?php

namespace App\Dto;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportDto{
	/**
	 * @var UploadedFile
	 * @Assert\NotBlank()
	 * @Assert\File(mimeTypes={"text/xml"})
	 */
	public $people;

	/**
	 * @var UploadedFile
	 * @Assert\NotBlank()
	 * @Assert\File(mimeTypes={"text/xml"})
	 */
	public $shiporders;

	/**
	 * @var boolean
	 */
	public $process;
}