<?php

namespace App\Controller\Api;


use App\Entity\Person;
use Dto\ShipordersDto;
use App\Entity\ShipOrder;
use App\Repository\ShipOrderRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;

/**
 * @Route("/shiporder")
 */
class ShipOrderController extends AbstractController{
	/**
	 * @Route("/", name="shiporder_index", methods={"GET"},defaults={"_format"="xml"})
	 * @OA\Response(
	 *     response="200",
	 *     description="Returns all shiporders",
	 *     @OA\XmlContent(
	 *          @OA\Schema(type="object",@OA\Property(property="shiporders",ref=@Model(type=ShipordersDto::class,groups={"default"})))
	 *     )
	 * )
	 */
	public function index(ShipOrderRepository $shipOrderRepository,SerializerInterface $serializer)
	:Response{

		return new Response(
			$serializer->serialize(
				new ShipordersDto($shipOrderRepository->findAll()),
				'xml',
				[
					XmlEncoder::ROOT_NODE_NAME => "shiporders",
					AbstractNormalizer::GROUPS => "default",
					AbstractNormalizer::CALLBACKS => [
						'person' => function(Person $person){
							return $person->getId();
						}
					]
				]
			)
		);
//	    return new Response();

//        return new JsonResponse($shiporderRepository->findAll());
	}

	/**
	 * @Route("/{id}", name="shiporder_show", methods={"GET"},defaults={"_format"="xml"})
	 * @OA\Response(
	 *     response="200",
	 *     description="Returns a shiporder",
	 *     @OA\XmlContent(
	 *          @OA\Schema(type="object",@OA\Property(property="shiporder",ref=@Model(type=ShipOrder::class,groups={"default"})))
	 *     )
	 * )
	 */
	public function show(ShipOrder $shiporder,SerializerInterface $serializer)
	:Response{
		return new Response(
			$serializer->serialize(
				$shiporder,
				'xml',
				[
					XmlEncoder::ROOT_NODE_NAME => "shiporder",
					AbstractNormalizer::GROUPS => "default",
					AbstractNormalizer::CALLBACKS => [
						'person' => function(Person $person){
							return $person->getId();
						}
					]
				]
			)
		);
	}
}
