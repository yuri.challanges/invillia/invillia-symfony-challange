<?php

namespace App\Controller\Api;


use App\Entity\Person;
use App\Dto\PeopleDto;
use App\Repository\PersonRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Annotations as OA;

/**
 * @Route("/person")
 */
class PersonController extends AbstractController{
	/**
	 * @Route("/", name="person_index", methods={"GET"},defaults={"_format"="xml"})
	 * @OA\Response(
	 *     response="200",
	 *     description="Returns all people",
	 *     @OA\XmlContent(
	 *          @OA\Schema(type="object",@OA\Property(property="people",ref=@Model(type=PeopleDto::class,groups={"default"})))
	 *     )
	 * )
	 */
	public function index(PersonRepository $personRepository,SerializerInterface $serializer)
	:Response{

		return new Response(
			$serializer->serialize(
				new PeopleDto($personRepository->findAll()),
				'xml',
				[
					XmlEncoder::ROOT_NODE_NAME => "people",
					AbstractNormalizer::GROUPS => "default",
				]
			)
		);
//	    return new Response();

//        return new JsonResponse($personRepository->findAll());
	}

	/**
	 * @Route("/{id}", name="person_show", methods={"GET"})
	 * @OA\Response(
	 *     response="200",
	 *     description="Returns a person",
	 *     @OA\XmlContent(
	 *          @OA\Schema(type="object",@OA\Property(property="person",ref=@Model(type=Person::class,groups={"default"})))
	 *     )
	 * )
	 */
	public function show(Person $person,SerializerInterface $serializer)
	:Response{
		return new Response(
			$serializer->serialize(
				$person,
				'xml',
				[
					XmlEncoder::ROOT_NODE_NAME => "person",
					AbstractNormalizer::GROUPS => "default",
				]
			)
		);
	}
}
