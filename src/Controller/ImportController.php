<?php

namespace App\Controller;


use App\Dto\ImportDto;
use App\Dto\PeopleDto;
use App\Entity\Person;
use App\Form\ImportType;
use App\Entity\PersonPhone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Serializer\PersonArrayXmlEnconderEncoder;
use App\Serializer\ShipOrderArrayXmlEnconderEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ImportController extends AbstractController{
	/**
	 * @Route("/", name="import")
	 */
	public function index(Request $request,PersonArrayXmlEnconderEncoder $personDeconder,ShipOrderArrayXmlEnconderEncoder $orderDecoder)
	:Response{
		$dto = new ImportDto();
		$form = $this->createForm(ImportType::class,$dto);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$dir = $this->getParameter('upload_dir');

			$peopleSavedFile = $dto->people->move("{$dir}/people",'people.xml');
			$shipordersSavedFile = $dto->shiporders->move("{$dir}/shiporders",'shiporders.xml');

			if($dto->process){
				$peopleFileContent = file_get_contents($peopleSavedFile);
    			$shipordersFileContent = file_get_contents($shipordersSavedFile);

    			$manager = $this->getDoctrine()->getManager();

				$people = $personDeconder->decode($peopleFileContent,'xml');
				foreach($people as $person){
					$manager->persist($person);
				}

				$manager->flush();

				$orders = $orderDecoder->decode($shipordersFileContent,'xml');
				foreach($orders as $order){
					$manager->persist($order);
				}

				$manager->flush();
			}
		}

		return $this->render(
			'import/index.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}
}
